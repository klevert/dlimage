# dlimage


Консольная утилита для многопоточного скачивания файлов, их обработки и сохранения в локальной файловой системе.

Обработка файлов заключается в генерации превью заданного максимального размера (с сохранением соотношения сторон) и перекодирования в формат jpeg.

Файл со списком url (urllist.txt) указывается параметром source в командной строке. Если файла не существует, выводится сообщение об ошибке.

Другие параметры командной строки:
--dir – выходной каталог. По умолчанию: thumbnails.
--size - размер выходных картинок (превью). По умолчанию: 100х100.
--threads – количество потоков. По умолчанию: один поток.

Пример использования:
pуthon main.py urllist.txt --dir= thumb/ --threads=4 --size=128x128 

