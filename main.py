import io
import os
import sys
import argparse
import logging
import threading
import re
import time
from functools import partial
from urllib.error import URLError
from urllib.request import Request, urlopen
from PIL import Image, UnidentifiedImageError
from typing import Generator
from multiprocessing.pool import ThreadPool

logging.basicConfig(level=logging.INFO, format='%(message)s')

logging.basicConfig(
    level=logging.INFO,
    format='%(message)s',
)
logger = logging.getLogger(__name__)


def loadresize(links, directory, size, l):
    """Resize image and save thumbnails in the directory"""
    try:
        response = urlopen(Request(links[1],
                                   headers={'User-Agent': 'Mozilla/5.0'}))

        length = response.length

        with Image.open(io.BytesIO(response.read())) as im:
            im.thumbnail(size)
            dir = os.path.join(directory, f"{links[0]:05}.jpeg")
            l.acquire()
            im.save(dir, "JPEG")
            l.release()
    except (URLError, UnidentifiedImageError):
        logging.error('Cannot get an image from %s', links[1])
    else:
        logging.info('Download complete %s ', links[1])
        return length


def urllist(source) -> Generator:
    """REad urls from local file
    :param source - name of local file
    :return generator of urls
    """
    return ((i, v.rstrip()) for i, v in enumerate(source))


def create_dir(target: str) -> str:
    """Create the directory for saving thumbnails
    :param target: str - name of the directory for saving thumbnails
    """
    path = os.path.join(os.getcwd(), target)
    try:
        os.makedirs(path)
    finally:
        return path


def valid_size(size: str) -> tuple:
    """valid size of images"""
    res = re.search(r"^(\d*)[xXхХ](\d*)$", size)
    if res:
        return tuple(int(x) for x in res.groups())
    else:
        raise argparse.ArgumentTypeError(f'{size} is not a valid size')


def parser_args() -> tuple:
    """Parser of arguments command line"""

    parser = argparse.ArgumentParser(description='Copy image file denoted by a URL to a local file.')
    parser.add_argument('source', type=argparse.FileType(),
                        help='List of urls')
    parser.add_argument('--dir', default='thumbnails',
                        help='Output directory')
    parser.add_argument('--threads', type=int, default=1,
                        help='The number of Thread objects')
    parser.add_argument('--size', default='100x100', type=valid_size,
                        help='Image size in pixels (default: 100x100).')

    args = parser.parse_args()
    print(args)

    return args.source, args.dir, args.threads, args.size


def timeit(func):
    """Python decorator to measure the execution time of methods"""

    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        res = func(*args, **kwargs)
        logging.info('Execution time %s seconds', (time.perf_counter() - start_time))
        return res

    return wrapper


@timeit
def main():
    """ Main entry point for script"""
    source, dir, threads, size = parser_args()
    directory = create_dir(dir)

    l = threading.Lock()

    pool = ThreadPool(processes=threads)
    results = pool.map(partial(loadresize, directory=directory, size=size, l=l), urllist(source))
    succeess = [v for v in results if v]

    logging.info('Total files: %s', len(succeess))
    logging.info('Error: %s', len(results) - len(succeess))
    logging.info('Size: %s bytes', sum(succeess))


if __name__ == '__main__':
    sys.exit(main())
